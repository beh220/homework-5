# Lehigh University CSE262 - Programming Languages - Homework 5

Solve the following ten questions regarding the Lambda Calculus.

## Question 1

Make all parentheses explicit in these λ- expressions:

1. (λp.pz) λq.w λw.wqzp
((λp.(p z)) ((λq.(w (λw.((((w q) z) p))))))

2. λp.pq λp.qp
(λp.((p q) (λp.(q p))))

## Question 2

In the following expressions say which, if any, variables are bound (and to which λ), and which are free.

1. λs.s z λq.s q
s is bound to the first λ, the z is free.  
q is bound to the second λ

2. (λs. s z) λq. w λw. w q z s
the first s is bound the first λ
the first z is free
the first w is unbounded
the second w and the last q are bound to the third λ
the final z and s are unbounded

3. (λs.s) (λq.qs)
the first s is bounded to the first  λ
the first q is bounded to the first λ, the second s is unbounded

4. λz. (((λs.sq) (λq.qz)) λz. (z z))
the first s is bounded to the second λ
the first q is unbounded
the second q is bounded to the second λ
the first z is unbounded
the final two zs are bounded to the last λ

## Question 3

Put the following expressions into beta normal form (use β-reduction as far as possible, α-conversion as needed) assuming left-association.

1. (λz.z) (λq.q q) (λs.s a)
replace z with λ
(λq.q q) (λs.s a)
replace q with (λs.s a)
(λs.s a) (λs.s a)
replace s with (λs.s a)
(λs.s a) a
replace s with a
a a

2. (λz.z) (λz.z z) (λz.z q)
z replaced with (λz.z z)
(λz.z z) (λz.z q)
replace z with (λz.z q)
(λz.z q)(λz.z q)
replace z with (λz.z q)
(λz.z q) q
replace z with q
q q

3. (λs.λq.s q q) (λa.a) b
replace s with (λa.a)
(λs.(λa.a)) q q) b
replace q with b
(λa.a) b b
replace a with b
b b

4. (λs.λq.s q q) (λq.q) q
replacing s with (λq.q)
(λs.(λq.q) q q) q
replace q with q
(λq.q) q q
q q

5. ((λs.s s) (λq.q)) (λq.q)
replace s with (λq.q)
((λq.q) (λq.q)) (λq.q)
replace q with (λq.q)
(λq.q) (λq.q)
replace q with (λq.q)
λq.q
 

## Question 4

1. Write the truth table for the or operator below.


2. The Church encoding for OR = (λp.λq.p p q)

Prove that this is a valid "or" function by showing that its output matches the truth table above. You will have 4 derivations. 
For the first derivation, show the long-hand solution (don't use T and F, use their definitions). 
For the other 3 you may use the symbols in place of the definitions. 
	P
Q	t	f
t	t	t
f	t	f

formal: p: t, q: t
(λp.λq.p p q)
((λa.λb.a) ==> true)
(λp.λq.p p q)
replace p with p
λq.p q
this is the same as the true definition


p: t, q: f
(λp.λq.p p q)
(λt.λf.t t f)
replace t with t
λf.t f
replace f
there is no f to replace
t (holds true)

p: f, q: t
(λp.λq.p p q)
(λf.λt.f f t)
replace f with f
(λt.f t)
nothing to repace to with
f t
t (holds true)


p: f, q: f
(λp.λq.p p q)
(λf.λf.f f f)
replace f with f
replace f with f
f (thus both f leads to false as a final result)

## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement
λx.λy.λz.x z a (not a)
it is similar to an if statment because it is atomically true or false.  the not operator flips the truth value of something, whereas a conditional can be used to perform an operation if true or false




## Instructions

1. Fork the relevant repository into your own namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Set your forked repository visibility to private. [Instructions](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)
3. Add user "LehighCSE262" as a member to the project with "maintainer" access-level. [Instructions](https://docs.gitlab.com/ee/user/project/members/#add-a-user). 
4. Clone your newly forked repository. [Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) 
5. Answer the questions here in the readme or in another document. Upload your solutions here to Gitlab.
